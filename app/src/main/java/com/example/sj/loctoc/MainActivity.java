package com.example.sj.loctoc;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.sj.loctoc.pojo.LocationCoordinates;
import com.example.sj.loctoc.pojo.permKey;
import com.example.sj.loctoc.pojo.tempkey;

import java.text.DecimalFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private myApiEndPoints endPoints;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressDialog= new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("fetching data from server");

        Button button= (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();

                retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.Base_Url)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                endPoints = retrofit.create(myApiEndPoints.class);

                Call<permKey> permKeyCall = endPoints.getPermKey(new tempkey(Constants.tempKey));

                permKeyCall.enqueue(new Callback<permKey>() {
                    @Override
                    public void onResponse(Call<permKey> call, Response<permKey> response) {
                      //  Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_LONG).show();

                        if (response.isSuccessful()) {
                            Constants.key = response.body();
                         //   Toast.makeText(getApplicationContext(), Constants.key.getPermKey(), Toast.LENGTH_LONG).show();

                            fetchLocations();
                        }

                    }


                    @Override
                    public void onFailure(Call<permKey> call, Throwable t) {

                      //  Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });



    }

    private void fetchLocations() {

        final LocationCoordinates[] coordinates = new LocationCoordinates[2];
        final int[] flag = new int[2];

        Call<LocationCoordinates> coordinatesCall = endPoints.getLocation(Constants.key);
        coordinatesCall.enqueue(new Callback<LocationCoordinates>() {
            @Override
            public void onResponse(Call<LocationCoordinates> call, Response<LocationCoordinates> response) {
                if (response.isSuccessful()) {

                    coordinates[0] = response.body();
                    flag[0] = 1;

                }
            }


            @Override
            public void onFailure(Call<LocationCoordinates> call, Throwable t) {
                coordinates[0] = null;
                flag[0] = 1;
            }
        });


        Call<LocationCoordinates> coordinatesCall2 = endPoints.getLocation(Constants.key);

        coordinatesCall2.enqueue(new Callback<LocationCoordinates>() {
            @Override
            public void onResponse(Call<LocationCoordinates> call, Response<LocationCoordinates> response) {
                if (response.isSuccessful()) {
                    coordinates[1] = response.body();
                    flag[1] = 1;
                }
            }

            @Override
            public void onFailure(Call<LocationCoordinates> call, Throwable t) {
                coordinates[1] = null;
                flag[1] = 1;
            }
        });


        final Handler ha = new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {


                //call function

                if (flag[0] == 1 && flag[1] == 1) {
                    calculateDistance(coordinates[0], coordinates[1]);
                } else
                    ha.postDelayed(this, 1000);
            }
        }, 1000);

    }

    private void calculateDistance(LocationCoordinates coordinates1, LocationCoordinates coordinates2) {

        if (progressDialog.isShowing())
            progressDialog.hide();
        if (coordinates1 == null || coordinates2 == null) {
            Toast.makeText(getApplicationContext(), "invalid location", Toast.LENGTH_SHORT).show();
            return;
        }

        final int R = 6371; // Radious of the earth
        Double lat1 = Double.parseDouble(coordinates1.getLatitude());
        Double lon1 = Double.parseDouble(coordinates1.getLongitude());

        Double lat2 = Double.parseDouble(coordinates2.getLatitude());
        Double lon2 = Double.parseDouble(coordinates2.getLongitude());
        Double latDistance = toRad(lat2 - lat1);
        Double lonDistance = toRad(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
                        Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        Double distance = R * c;

        DecimalFormat df = new DecimalFormat("#.00");
        String Formated = df.format(distance);
        Toast.makeText(getApplicationContext(), " The distance between two lat and long is  " + Formated +" Km", Toast.LENGTH_LONG).show();

        System.out.println("The distance between two lat and long is::" + distance);

    }

    private double toRad(double v) {
        return v * Math.PI / 180;
    }


}
