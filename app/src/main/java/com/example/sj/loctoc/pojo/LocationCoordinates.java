package com.example.sj.loctoc.pojo;

/**
 * Created by sj on 11/4/17.
 */

public class LocationCoordinates {

    String  latitude;
    String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
