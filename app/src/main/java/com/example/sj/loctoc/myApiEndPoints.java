package com.example.sj.loctoc;

import com.example.sj.loctoc.pojo.LocationCoordinates;
import com.example.sj.loctoc.pojo.permKey;
import com.example.sj.loctoc.pojo.tempkey;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by sj on 11/4/17.
 */

public interface myApiEndPoints {

    @POST("getKey")
    Call<permKey> getPermKey(@Body tempkey user);

    @POST("getCoordinates")
    Call<LocationCoordinates> getLocation(@Body permKey user);


}
